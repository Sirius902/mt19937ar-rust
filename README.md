# MT19937ar Rust
A port of the Mersenne Twister library found at http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html to Rust. This port is also intended to expose the internal state of the generator. Source can be found here: http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/emt19937ar.html.

# Usage
## Cargo.toml
Add this line to the Cargo.toml file.
```
mt19937ar = { git = "https://gitlab.com/Sirius902/mt19937ar-rust" }
```

## Sample Program
```rust
extern crate mt19937ar;

use mt19937ar::MT19937;

fn main() {
    // Create and seed Mersenne Twiser with u32
    let mut mt = MT19937::from(0x9FBF1);
    // Generate random u32
    let a = mt.genrand_int32();
    // Generate random i32
    let b = mt.genrand_int31();
    // Generate random f64 on the interval [0,1]
    let c = mt.genrand_real1();
    // Generate random f64 on the interval [0,1)
    let d = mt.genrand_real2();
    
    println!("{:08X}", a);
    println!("{:08X}", b);
    println!("{}", c);
    println!("{}", d);
}
```

# Copyright Information and Disclaimer
```
Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
All rights reserved.
Copyright (C) 2005, Mutsuo Saito,
All rights reserved.
```

```
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```