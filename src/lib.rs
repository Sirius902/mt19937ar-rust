mod mt19937ar;
pub use self::mt19937ar::*;

#[cfg(test)]
mod tests {
    use super::*;

    pub const RECOM_HD_SEED: u32 = 0x0009FBF1;

    #[test]
    fn test_genrand() {
        let mut mt = MT19937::from(RECOM_HD_SEED);
        
        assert_eq!(mt.genrand_int32(), 0x3B63F5F0, "Failed to generate correct number!");
        assert_eq!(mt.genrand_int31(), 0x2551CF00, "Failed to generate correct number!");
        assert_eq!(mt.genrand_int31(), 0x5DC38DD3, "Failed to generate correct number!");
        assert_eq!(mt.genrand_int31(), 0x761D6A7F, "Failed to generate correct number!");
        assert_eq!(mt.mti(), 4);

        for _ in 0..4 {
            let _ = mt.genrand_int32();
        }

        assert_eq!(mt.mti(), 8);
        let fp = mt.genrand_real1();
        assert!(fp < 0.860 && fp > 0.859, "Failure to generate correct number!");
    }
}
