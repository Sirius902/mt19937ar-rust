/*

Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
All rights reserved.
Copyright (C) 2005, Mutsuo Saito,
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

use std::convert::TryFrom;
use std::num::Wrapping;

// Period parameters
pub const N: usize = 624;
pub const M: usize = 397;

/// constant vector a
pub const MATRIX_A: u32 = 0x9908b0df;

/// most significant w-r bits
pub const UPPER_MASK: u32 = 0x80000000;

/// least significant r bits
pub const LOWER_MASK: u32 = 0x7fffffff;

#[derive(Copy, Clone)]
pub struct MT19937 {
    /// the array for the state vector
    mt: [u32; N],
    /// mti==N+1 means mt[N] is not initialized
    mti: usize
}

impl MT19937 {
    /// Returns an uninitialized generator.
    /// 
    /// Initialize by calling either `MT19937::init_genrand`, `MT19937::init_by_array`.
    /// 
    /// It's recommended to instead use the From trait implemented for seeds of type
    /// `u32` and `&[u32]` to create a new generator.
    pub fn uninitialized() -> Self {
        MT19937 {
            mt: [0; N],
            mti: N + 1
        }
    }

    /// Get the current internal state vector of the generator.
    pub fn state_vector(&self) -> [u32; N] {
        self.mt
    }

    /// Get the current index of the generator.
    pub fn mti(&self) -> usize {
        self.mti
    }

    /// initializes mt[N] with a seed
    pub fn init_genrand(&mut self, s: u32) {
        self.mt[0] = s & 0xffffffff;
        self.mti = 1;

        while self.mti < N {
            self.mt[self.mti] =
                (Wrapping(1812433253) *
                    Wrapping(self.mt[self.mti-1] ^ (self.mt[self.mti-1] >> 30)) +
                    Wrapping(u32::try_from(self.mti).unwrap())).0;

            self.mt[self.mti] &= 0xffffffff;

            self.mti += 1;
        }
    }

    /// initialize by an array
    pub fn init_by_array(&mut self, init_key: &[u32]) {
        let mut i = 1;
        let mut j = 0;
        let mut k = if N > init_key.len() {
            N
        } else {
            init_key.len()
        };

        self.init_genrand(19650218);

        while k > 0 {
            self.mt[i] =
                (Wrapping(self.mt[i] ^ (self.mt[i-1] ^ (self.mt[i-1] >> 30))) *
                    Wrapping(1664525) +
                    Wrapping(init_key[j]) +
                    Wrapping(u32::try_from(j).unwrap())).0;

            self.mt[i] &= 0xffffffff;
            i += 1;
            j += 1;

            if i >= N {
                self.mt[0] = self.mt[N - 1];
                i = 1;
            }

            if j >= init_key.len() { j = 0; }

            k -= 1;
        }

        k = N - 1;
        while k > 0 {
            self.mt[i] =
                (Wrapping(self.mt[i] ^ (self.mt[i - 1] ^ (self.mt[i - 1] >> 30))) *
                    Wrapping(1566083941) -
                    Wrapping(u32::try_from(i).unwrap())).0;

            self.mt[i] &= 0xffffffff;
            i += 1;
            if i >= N {
                self.mt[0] = self.mt[N - 1];
                i = 1;
            }

            k -= 1;
        }

        self.mt[0] = 0x80000000;
    }

    /// generates a random number on [0,0xffffffff]-interval
    pub fn genrand_int32(&mut self) -> u32 {
        let mut kk;
        let mut y;
        let mag01: [u32; 2] = [0x0, MATRIX_A];

        if self.mti >= N {
            if self.mti == N + 1 {
                self.init_genrand(5489);
            }

            kk = 0;
            while kk < N - M {
                y = (self.mt[kk] & UPPER_MASK) | (self.mt[kk+1] & LOWER_MASK);
                self.mt[kk] = self.mt[kk + M] ^ (y >> 1) ^ mag01[usize::try_from(y & 0x1).unwrap()];
                
                kk += 1;
            }

            while kk < N - 1 {
                y = (self.mt[kk] & UPPER_MASK) | (self.mt[kk+1] & LOWER_MASK);
                self.mt[kk] = self.mt[kk + M - N] ^ (y >> 1) ^ mag01[usize::try_from(y & 0x1).unwrap()];
            
                kk += 1;
            }
            y = (self.mt[N - 1] & UPPER_MASK) | (self.mt[0] & LOWER_MASK);
            self.mt[N - 1] = self.mt[M - 1] ^ (y >> 1) ^ mag01[usize::try_from(y & 0x1).unwrap()];

            self.mti = 0;
        }

        y = self.mt[self.mti];
        self.mti += 1;

        y ^= y >> 11;
        y ^= (y << 7) & 0x9d2c5680;
        y ^= (y << 15) & 0xefc60000;
        y ^= y >> 18;

        y
    }

    /// generates a random number on [0,0x7fffffff]-interval
    pub fn genrand_int31(&mut self) -> i32 {
        (self.genrand_int32() >> 1) as i32
    }

    /// generates a random number on [0,1]-real-interval
    pub fn genrand_real1(&mut self) -> f64 {
        f64::from(self.genrand_int32()) * (1.0 / 4294967295.0)
    }

    /// generates a random number on [0,1)-real-interval
    pub fn genrand_real2(&mut self) -> f64 {
        f64::from(self.genrand_int32()) * (1.0 / 4294967296.0)
    }

    /// generates a random number on (0,1)-real-interval
    pub fn genrand_real3(&mut self) -> f64 {
        (f64::from(self.genrand_int32()) + 0.5) * (1.0 / 4294967296.0)
    }

    /// generates a random number on [0,1) with 53-bit resolution
    pub fn genrand_res53(&mut self) -> f64 {
        let a = f64::from(self.genrand_int32() >> 5);
        let b = f64::from(self.genrand_int32() >> 6);

        (a * 67108864.0 + b) * (1.0 / 9007199254740992.0)
    }
}

impl From<u32> for MT19937 {
    fn from(s: u32) -> MT19937 {
        let mut mt = MT19937::uninitialized();
        mt.init_genrand(s);
        mt
    }
}

impl From<&[u32]> for MT19937 {
    fn from(init_key: &[u32]) -> MT19937 {
        let mut mt = MT19937::uninitialized();
        mt.init_by_array(init_key);
        mt
    }
}
